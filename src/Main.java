import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) throws IOException {

        Scanner scanner = new Scanner(System.in);
        int Question_number;

        while (true) {
            while (true) {
                System.out.print("請輸入數字1~8來選擇題目:");
                Question_number = scanner.nextInt();
                if (Question_number == 1 || Question_number == 2 || Question_number == 3 || Question_number == 4
                        || Question_number == 5 || Question_number == 6 || Question_number == 7 || Question_number == 8) {
                    break;
                }
                System.out.println("錯誤的輸入");
            }
            if (Question_number == 0)
                break;
            else if (Question_number == 1)
                Quiz_1();
            else if (Question_number == 2)
                Quiz_2();
            else if (Question_number == 3)
                Quiz_3();
            else if (Question_number == 4)
                Quiz_4();
            else if (Question_number == 5)
                Quiz_5();
            else if (Question_number == 6)
                Quiz_6();
            else if (Question_number == 7)
                Quiz_7();
            else if (Question_number == 8)
                Quiz_8();
        }
    }

    private static void Quiz_1() {
        System.out.println("題目一:");
        System.out.print("請輸入100-999數字:");
        Scanner scanner = new Scanner(System.in);
        int num;
        num = scanner.nextInt();
        while (true) {
            if (num >= 100 && num < 1000)
                break;
            else {
                System.out.print("錯誤，請重新輸入:");
                num = scanner.nextInt();
            }

        }
        int sum = 0;
        int pro = 1;
        int sub = 0;
        while (num != 0) {
            sum += num % 10;
            pro *= num % 10;
            if (num / 10 != 0)
                sub -= num % 10;
            else
                sub += num % 10;
            num = num / 10;
        }
        System.out.println("每位數和:" + sum);
        System.out.println("每位數積:" + pro);
        System.out.println("每位數差:" + sub);
    }

    private static void Quiz_2() {
        System.out.println("題目二:");
        int i = 0;
        i = (int) (Math.random() * (20 - 10 + 1)) + 10;
        System.out.println("產生" + i + "個0-999數字");
        for (int j = 0; j < i; j++) {
            int num = 0;
            num = (int) (Math.random() * (999));
            System.out.println("數字" + (j + 1) + ":" + num);
            int sum = 0;
            int pro = 1;
            int sub = 0;
            int tmp = num;
            boolean down_100 = false;
            while (tmp != 0) {
                if (num < 100) {
                    down_100 = true;
                    break;
                }
                sum += tmp % 10;
                pro *= tmp % 10;
                if (tmp / 10 != 0)
                    sub -= tmp % 10;
                else
                    sub += tmp % 10;
                tmp = tmp / 10;
            }
            if (down_100 == true) {
                System.out.println("小於100");
                System.out.println(" ");
            } else {
                System.out.println("每位數和:" + sum);
                System.out.println("每位數積:" + pro);
                System.out.println("每位數差:" + sub);
                System.out.println(" ");
            }
        }
    }

    private static void Quiz_3() {
        System.out.println("題目三:");
        System.out.print("請輸入多個整數(不為零)，以零為結束:");
        Scanner scanner = new Scanner(System.in);
        int[] arr = new int[999];
        int i = 0;
        while (true) {
            arr[i] = scanner.nextInt();
            if (arr[i] == 0)
                break;
            i++;
        }
        int sum = 0;
        int nesum = 0;
        int posum = 0;
        double mean = 0;
        for (int j = 0; arr[j] != 0; j++) {
            sum += arr[j];
            if (arr[j] < 0) nesum++;
            if (arr[j] > 0) posum++;
        }
        mean = Double.valueOf(sum) / i;
        System.out.println("正負數個數: 正:" + posum + " 負:" + nesum);
        System.out.println("總和:" + sum + " 平均:" + mean);
    }

    private static void Quiz_4() throws IOException {

        System.out.println("題目四:");
        int i = 0;
        i = (int) (Math.random() * (20 - 10 + 1)) + 10;
        System.out.println("產生" + i + "個-10000~10000數字");
        int[] arr = new int[20];
        int sum = 0;
        int nesum = 0;
        int posum = 0;
        double mean = 0;
        for (int j = 0; j < i; j++) {
            arr[j] = (int) (Math.random() * 20000) - 10000;
            sum += arr[j];
            if (arr[j] < 0) nesum++;
            if (arr[j] > 0) posum++;
        }
        mean = Double.valueOf(sum) / i;


        File writename = new File("output.txt");
        writename.createNewFile();
        BufferedWriter out = new BufferedWriter(new FileWriter(writename));
        out.write("正負數個數: 正:" + posum + " 負:" + nesum + "\r\n" + "總和:" + sum + " 平均:" + mean + "\r\n");
        out.flush();
        out.close();
    }

    private static void Quiz_5() {
        System.out.println("題目五:");
        System.out.print("請輸入十個整數:");
        Scanner scanner = new Scanner(System.in);
        int[] arr = new int[999];
        for (int i = 0; i < 10; i++) {
            arr[i] = scanner.nextInt();
        }
        int min = arr[0];
        int count = 1;
        for (int i = 1; i < 10; i++) {
            if (arr[i] < min) {
                min = arr[i];
                count = 0;
            }
            if (arr[i] == min)
                count++;
        }
        System.out.println("最小值:" + min + "   共" + count + "次");
    }

    private static void Quiz_6() {
        System.out.println("題目六:");
        int i = 0;
        i = (int) (Math.random() * (10)) + 1;
        System.out.print("請輸入" + i + "個整數:");
        Scanner scanner = new Scanner(System.in);
        int[] arr = new int[999];
        for (int j = 0; j < i; j++) {
            arr[j] = scanner.nextInt();
        }
        int min = arr[0];
        int countMin = 1;
        int max = arr[0];
        int countMax = 1;
        for (int j = 1; j < i; j++) {
            if (arr[j] < min) {
                min = arr[j];
                countMin = 0;
            }
            if (arr[j] == min)
                countMin++;
            if (arr[j] > max) {
                max = arr[j];
                countMax = 0;
            }
            if (arr[j] == max)
                countMax++;
        }
        System.out.println("最小值:" + min + "   共" + countMin + "次");
        System.out.println("最小值:" + max + "   共" + countMax + "次");
    }

    private static void Quiz_7() {
        System.out.println("題目七:");
        System.out.print("請輸入三個數字:");
        Scanner scanner = new Scanner(System.in);
        double[] arr = new double[3];
        for (int j = 0; j < 3; j++) {
            arr[j] = scanner.nextDouble();
        }

        for(int i=0;i<3;i++){
            for (int j=i;j<3;j++){
                if(arr[i]>arr[j]){
                    double tmp=arr[i];
                    arr[i]=arr[j];
                    arr[j]=tmp;
                }
            }
        }
        System.out.print("排序: ");
        for (int i=0;i<3;i++)
            System.out.print(arr[i]+" ");

        System.out.println(" ");
    }
    private static void Quiz_8() {
    }
}
